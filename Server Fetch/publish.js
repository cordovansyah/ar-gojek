const Pusher = require('pusher');

const pusher = new Pusher({
  appId: '615882',
  key: '70cdf5e5d41623d28576',
  secret: '86e337cefb39fa82399d',
  cluster: 'ap1',
  encrypted: true
});

const locations = [
    {latitude: "-6.887667", longitude: "107.610222", heading: "227° 53′ 17″"},
];

locations.forEach((loc, index) => {
    setTimeout(() => {
        console.log(loc);
        pusher.trigger('private-channel', 'client-new-location', loc);
    }, 2000*index);
});