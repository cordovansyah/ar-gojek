//
//  FloatingPoint+Extension.swift
//  AR Gojek
//
//  Created by octagon studio on 07/10/18.
//  Copyright © 2018 Cordova. All rights reserved.
//

import Foundation

extension FloatingPoint {
    func toRadians() -> Self {
        return self * .pi / 180
    }
    
    func toDegrees() -> Self {
        return self * 180 / .pi
    }
}
