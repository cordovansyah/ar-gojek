//
//  ViewController.swift
//  AR Gojek
//
//  Created by Cordova on 07/10/18.
//  Copyright © 2018 Cordova. All rights reserved.
//

import UIKit
import SceneKit
import CoreLocation
import PusherSwift
import MapKit


class ViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {

   
    @IBOutlet weak var mapView: MKMapView!
    
    var locationManager = CLLocationManager()
    var userHeading = 0.0
    var userLocation = CLLocation()
    
    
    let pusher = Pusher(
        key: "70cdf5e5d41623d28576",
        options: PusherClientOptions(
            authMethod: .inline(secret: "86e337cefb39fa82399d"),
            host: .cluster("ap1")
        )
    )
    
     var channel: PusherChannel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        darkNIB()
        listenToCoordinateUpdates()

    }

    
    func listenToCoordinateUpdates(){
        
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
            locationManager.startUpdatingLocation()
            locationManager.distanceFilter = 2.0
        }
        
        mapView.delegate = self
        mapView.mapType = .standard
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        
        if let coor = mapView.userLocation.location?.coordinate{
            mapView.setCenter(coor, animated: true)
        }
        
         channel = pusher.subscribe("private-channel")
         pusher.connect()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            userLocation = location
            
            self.connectToServer(manager)
        }
    }
    
    func connectToServer(_ manager: CLLocationManager){
        let channel = pusher.subscribe("private-channel")
        
        let _ = channel.bind(eventName: "client-new-location", callback: {(data: Any?) -> Void in
            if let data = data as? [String: AnyObject]{
                if let latitude = Double(data["latitude"] as! String),
                    let longitude = Double(data["longitude"] as! String),
                    let heading = Double(data["heading"] as! String){
                    self.userHeading = heading
                    
                    let locValue:CLLocationCoordinate2D = manager.location!.coordinate
                    self.mapView.mapType = MKMapType.standard
                    
                    let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
                    
                    let region = MKCoordinateRegion(center: locValue, span: span)
                    self.mapView.setRegion(region, animated: true)
                    
                    let annotation = MKPointAnnotation()
                    
                    annotation.coordinate = locValue
                    annotation.title = "Cordova"
                    annotation.subtitle = "current location"
                    
                    self.mapView.addAnnotation(annotation)
                    self.updateLocation(latitude, longitude)
                }
            }
        })
        pusher.connect()
    }
    
    func updateLocation(_ latitude: Double, _ longitude: Double){
       let _ = CLLocation(latitude: latitude, longitude: longitude)
    }
    
    
    //MARK: - CUSTOM UI ELEMENT and INTERACTION DESIGN
    //1. Dark Status Bar
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    //2. Dark Navigation Item Bar and Text
    func darkNIB(){
        navigationController?.navigationBar.barTintColor = UIColor.black
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
    }

   
}

