//
//  ARViewController.swift
//  AR Gojek
//
//  Created by octagon studio on 07/10/18.
//  Copyright © 2018 Cordova. All rights reserved.
//

import UIKit
import ARKit
import SceneKit
import CoreLocation
import PusherSwift

class ARViewController: UIViewController, ARSCNViewDelegate, CLLocationManagerDelegate {
    @IBOutlet weak var sceneView: ARSCNView!
    
    //MARK: - UI ELEMENTS
    //Do : Store all artboard and code based UI elements
    
    let textContainer = UIView(frame: CGRect(x: 16, y: 500, width: 343, height: 84))
    var statusText = UILabel(frame: CGRect(x: 16, y: 500, width: 343, height: 84))
    
    
    //MARK: - REQUEST USER LOCATION
    //Do : Request the user location and another variable to store it
    let locationManager = CLLocationManager()
    var userLocation = CLLocation()
    
    //MARK: - STORE DIRECTION FOR DRIVERS, DISTANCE BETWEEN U&D AND APP STATUS
    //Do : Make sure driver knows where to head to users and provide client based ( UI ) status
    var heading: Double! = 0.0
    var distance: Float! = 0.0 {
        didSet {
            setStatus()
        }
    }
    var status: String! {
        didSet{
            setStatus()
        }
    }
    
    //MARK: - STORE 3D MODEL ROOT NODE ( MOTORCYCLE )
    //Do: Define the object
    var originalTransform: SCNMatrix4!
    var modelNode: SCNNode!
    let rootNodeName = "Car"
    
    //MARK: - RECEIVE UPDATES FROM SERVER
    //Do : Push and receive updates based on user's order
    let pusher = Pusher(
        key: "70cdf5e5d41623d28576",
        options: PusherClientOptions (
            authMethod: .inline(secret:"86e337cefb39fa82399d"),
            host: .cluster("ap1")
        )
    )
    var channel: PusherChannel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        darkNib()
        sceneInit()
        locationInitialization()
        drawTextContainer()
        setStatus()
        
        
        status = "Memindai lokasi anda..."
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        initAR()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        pauseAR()
    }
    
    
    //MARK: - METHOD MANAGEMENT
    
    //MARK: - UI ELEMENT METHODS
    //Do : Draw The Layer Methods
    func drawTextContainer(){
        textContainer.backgroundColor = UIColor.black.withAlphaComponent(0.60)
        textContainer.isOpaque = false
        textContainer.layer.cornerRadius = 25
        self.view.addSubview(textContainer)
    }
    
    //Do: Set Status Text
    func setStatus(){
        var orderStatus = "Status"
        orderStatus += " 🚘 Distance: \(String(format:"%.2f m", distance ))"
        
        statusText.textAlignment = .center
        statusText.text = orderStatus
        statusText.textColor = UIColor.white
        statusText.font = UIFont.systemFont(ofSize: 20, weight: UIFont.Weight.light)
        self.view.addSubview(statusText)
        
    }
    
    //MARK: - AUGMENTED REALITY GENERAL SETUP
    func initAR(){
        let configuration = ARWorldTrackingConfiguration()
        configuration.worldAlignment = .gravityAndHeading
        sceneView.session.run(configuration)
    }
    func pauseAR(){
        sceneView.session.pause()
    }
    
    //MARK: - CUSTOM UI ELEMENT DESIGN
    //1. Dark Navigation Item Bar Style
    func darkNib(){
        navigationController?.navigationBar.barTintColor = UIColor.black
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }

    
    //MARK : - SEGUES
    //1. Back from AR Viewer to Map View
    @IBAction func mapMode(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: - LOCATION MANAGER & SCENE SETUP
    
    func sceneInit(){
        sceneView.delegate = self
        let scene = SCNScene()
        sceneView.scene = scene
    }
    //MARK: - Initialize Location and Manage User's Behavior on GPS Usage
    func locationInitialization(){
        //Start Location Service
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.requestLocation()
        }
    }
    
    //MARK: - Update user locations and push update notifications to the server
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            userLocation = location
            status = "Menghubungkan dengan server..."
            self.connectToServer()
        }
    }
    
    func connectToServer(){
        let channel = pusher.subscribe("private-channel")
        let _ = channel.bind(eventName: "client-new-location", callback: {(data: Any?) -> Void in
            if let data = data as? [String: AnyObject]{
                if let latitude = Double(data["latitude"] as! String),
                    let longitude = Double(data["longitude"] as! String),
                    let heading = Double(data["heading"] as! String){
                    self.status = "Lokasi driver telah diterima"
                    self.heading = heading
                    self.updateLocation(latitude, longitude)
                }
            }
        })
        pusher.connect()
        status = "Driver sedang dalam proses menerima lokasi anda"
    }
    
    //MARK: - Calculate The Distance Between User and Driver
    func updateLocation(_ latitude: Double, _ longitude: Double){
        let location = CLLocation(latitude: latitude, longitude: longitude)
        self.distance = Float(location.distance(from: self.userLocation))
        
        if self.modelNode == nil {
            let modelScene = SCNScene(named: "art.scnassets/Car.dae")!
            self.modelNode = modelScene.rootNode.childNode(withName: rootNodeName, recursively: true)!
            
            // Automatic Object (Model) Rotation
            let (minBox, maxBox) = self.modelNode.boundingBox
            self.modelNode.pivot = SCNMatrix4MakeTranslation(0, (maxBox.y - minBox.y) / 2, 0)
            
            self.originalTransform = self.modelNode.transform
            modelPosition(location)
            sceneView.scene.rootNode.addChildNode(self.modelNode)
            
            //Add Arrow to the object
            let arrow = drawArrowNode("▿".image()!)
            arrow.position = SCNVector3Make(0, 4, 0)
            self.modelNode.addChildNode(arrow)
        } else {
            SCNTransaction.begin()
            SCNTransaction.animationDuration = 1.0
            modelPosition(location)
            SCNTransaction.commit() //end animation
        }
    }
    
    // MARK: - Define The Arrow Object
    func drawArrowNode(_ image: UIImage) -> SCNNode {
        let plane = SCNPlane(width: 10, height: 10)
        plane.firstMaterial!.diffuse.contents = image
        let node = SCNNode(geometry: plane)
        node.constraints = [SCNBillboardConstraint()]
        return node
    }
    
    func modelPosition(_ location: CLLocation){
        self.modelNode.transform = rotateNode(Float(-1 * (self.heading - 180).toRadians()), self.originalTransform)
        self.modelNode.position = translateNode(location)
        self.modelNode.scale = scaleNode(location)
    }
    
    func rotateNode(_ angleInRadians: Float, _ transform: SCNMatrix4) -> SCNMatrix4 {
        let rotation = SCNMatrix4MakeRotation(angleInRadians, 0, 1, 0)
        return SCNMatrix4Mult(transform, rotation)
    }
    
    func scaleNode(_ location: CLLocation) -> SCNVector3 {
        let scale = min(max(Float(1000/distance), 1.5), 3)
        return SCNVector3(x: scale, y:scale, z: scale)
    }
    
    func translateNode(_ location: CLLocation) -> SCNVector3 {
        let locationTransform =
            transformMatrix(matrix_identity_float4x4, userLocation, location)
        return positionFromTransform(locationTransform)
    }
    
    func positionFromTransform(_ transform: simd_float4x4) -> SCNVector3 {
        return SCNVector3Make(transform.columns.3.x, transform.columns.3.y, transform.columns.3.z)
        
    }
    // MARK: - MATH
    func transformMatrix(_ matrix: simd_float4x4, _ originLocation: CLLocation, _ driverLocation: CLLocation) -> simd_float4x4 {
        let bearing = bearingBetweenLocations(userLocation, driverLocation)
        let rotationMatrix = rotateAroundY(matrix_identity_float4x4, Float(bearing))
        
        let position = vector_float4(0.0, 0.0, -distance, 0.0)
        let translationMatrix = getTranslationMatrix(matrix_identity_float4x4, position)
        
        let transformMatrix = simd_mul(rotationMatrix, translationMatrix)
        
        return simd_mul(matrix, transformMatrix)
    }
    
    func getTranslationMatrix(_ matrix: simd_float4x4, _ translation : vector_float4) -> simd_float4x4 {
        var matrix = matrix
        matrix.columns.3 = translation
        return matrix
    }
    
    func rotateAroundY(_ matrix: simd_float4x4, _ degrees: Float) -> simd_float4x4 {
        var matrix = matrix
        
        matrix.columns.0.x = cos(degrees)
        matrix.columns.0.z = -sin(degrees)
        
        matrix.columns.2.x = sin(degrees)
        matrix.columns.2.z = cos(degrees)
        return matrix.inverse
    }
    
    func bearingBetweenLocations(_ originLocation: CLLocation, _ driverLocation: CLLocation) -> Double {
        let lat1 = originLocation.coordinate.latitude.toRadians()
        let lon1 = originLocation.coordinate.longitude.toRadians()
        
        let lat2 = driverLocation.coordinate.latitude.toRadians()
        let lon2 = driverLocation.coordinate.longitude.toRadians()
        
        let longitudeDiff = lon2 - lon1
        
        let y = sin(longitudeDiff) * cos(lat2);
        let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(longitudeDiff);
        
        return atan2(y, x)
    }

}
