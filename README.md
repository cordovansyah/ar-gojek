# Augmented Reality Ride Hailing App ( Drivers )

How if I am able to track my driver or users whom order ride services by both static 2D maps and as well as 
the power of augmented reality through my mobile device ?. This is the project that I did recently to expand the possibility of
real time AR based tracking app that can possibly be implemented in driver's helmet in near future.

# Motivation
I worked in an AR based software company in Bandung, and I am flabbergasted with how the technology works and it's impact for users in
in wider range of business perspective in the fourth industrial revolution. Thus, couple of months ago, I decided to conduct self-learning
with Apple's ARKit technology and thus I am become more fascinated and decided to dig deeper and build more stuffs, and eventually combined 
with other technology lineups such as CoreML, Core Location, Scene Kit, and many more. 

This project is part of my own curiosity, and for sure there are lots of rooms to improve !

### Prerequisites
```
Please install Pusher Swift Podfile before using this app.
```

